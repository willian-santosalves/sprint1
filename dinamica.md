# Como um QA pode gerar qualidade em um projeto

Resultado da discussão entre:
-   Arthur Ian de Melo Sousa
-   Kenny Keisuke Hobo
-   Fernanda Dias Martins de Oliveira
-   Willian dos Santos Alves

É importante que o QA tenha em mente que gerar qualidade não se dá apenas testando a aplicação de qualquer forma. É necessário que principalmente seja garantida a solicitação do cliente, tendo em mente que muitas vezes o cliente pode não saber explicar exatamente o que ele deseja, tornando necessário o acompanhamento do projeto.

O profissional de QA também deve se preocupar com: 
-
-   Se os Requisitos da aplicação estão de acordo com o negocio, ou seja, se ele está ***funcionalmente adequado***.
-   Se o produto é confiável e não irá deixar o cliente na mão durante seu uso.
-   Redução de riscos no uso do produto.
-   Garantia que o produto está conforme o contrato.

## Conhecimento necessário

Para que o QA possa realmente gerar qualidade, é necessário que ele possua conhecimento em diversos âmbitos, como por exemplo:

-   Tipos de teste - estático x dinâmico
-   Depuração de código e testes de confirmação
-   Ferramentas de teste (Insomnia, Postman, K6, Jmeter e etc)
-   7 Fundamentos do teste de software

Também é necessário que ele saiba trabalhar em uma equipe preparada para realizar essas atividades e que saibam agir em grupo. Muitas vezes garantir a qualidade de um produto leva uma grande quantidade de tempo, que pode ser diminuida a partir do trabalho em equipe.

Também é a partir desse trabalho em equipe que o QA pode ter melhor desempenho em suas atividades, como por exemplo ao receber o feedback de um cliente por meio do Product Owner em uma reunião de equipe.

***O teste que encontra defeitos cria a oportunidade de melhorar a qualidade do produto***

## ***Teste != Qualidade***

É importante que o QA saiba que testar uma aplicação não necessáriamente garante a sua qualidade.

O teste só aumenta a qualidade do software quando:
- Defeitos são encontrados e corrigidos;
- Ocorre a verificação da conformidade dos requistos funcionais;
- Requisitos não-funcionais, ou seja, técnicos (confiabilidade, usabilidade, escalabilidade, etc) são verificados.

> Seguindo a IEC/ISO 25010

O tester age diretamente no produto e o QA age no processo de produção

## Psicologia do Teste

É essencial que o QA saiba que o desenvolvimento e os testes envolvem muito os seres humanos, portanto a ***psicologia humana*** tem efeito nos testes de software.

- Desenvolvedor
    -   Projeta e constroi um produto
    -   Tem dificuldade de aceitar que seu código possa estar errado
- Testador
    -   Verifica e Valida o produto. Encontra defeitos antes da liberação.
    -   Pode ser visto como destrutivo e tem o potencial de ofender aqueles cujo trabalho está sobre revisão

Por esse motivo é importante que o tester saiba como se comunicar com o desenvolvedor da maneira correta, para que haja maior eficiência no processo de testes.

## Princípio de Pareto

Durante a busca por erros, é comum que os profissionais de QA se deparem com a concentração de bugs em um módulo específico do produto. O princípio de pareto diz que cerca de 80% do resultado é gerado por cerca de 20% do esforço, e isso também pode ser aplicado no teste de software.

## Exemplo: Segmento de e-commerce

Em diversos ambientes web de compra e venda temos períodos de maior acesso, como por exemplo dia dos namorados (em lojas de chocolate), e varejo (em black friday), sendo assim é bem importante que o QA saiba bem sobre o negócio em que ele está trabalhando para que possa previnir diversas falhas na aplicação

