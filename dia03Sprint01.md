# Backlog da Sprint  01

## Dia 03 - MasterClass | Fundamnetos do Teste de Software

### Fundamentos do Teste de Software

### ***Início Rápido em Teste e QA***

O Curso comecça com uma visão geral e abrangente sobre a carreira em Teste de Sofware e QA.

Elenca os níveis de profissionais, como:
-   Estágio
-   Trainee
-   Junior
-   Pleno
-   Sênior e
-   Especialista

Também comenta sobre os cargos de liderança que o profissional pode passar ao longo da carreira, como:
-   Coordenador
-   Líder
-   Gerente
-   Diretor e, quem sabe
-   CEO

Como todas as carreiras da área de T.I, o profissional de Teste e QA, precisa se manter atualizado sobre as tecnologias. Ese aprendizado contínuo é necessário, vito que, 1 ano sem estudar ou trabalhar na T.I, equivalem a 7 de desenvolvimento e avanços tecnológicos. Isso é ruim porque o profissional sente uma defasagem quando volta a ativa.

Para manter-se atualizado o profissional pode ler artigos, notícias, participar de grupos sobre Teste e QA, cursos rápidos, conteúdos do youtube, Pós Graduação e muito mais.

Foi abordado sobre as vagas que há no mundo todo, no Brasil, onde procurar e encontrar essa vagas. 

Ainda, foi falado sobre os regimes de contratação como PJ (Pessoa Jurídica), CLT (sobre as leis do trabalho) e até deu dicas e exemplos de empreendedorismo.

Por fim, falou sobre os setores de negócios que contratam os profissionais de Teste e QA, que são eles:
-   T.I
-   Telecomunicações
-   Financeiro
-   Governo (Estatal)
-   Transporte
-   Saúde
-   Comércio e
-   Industrial entre tantos outros.