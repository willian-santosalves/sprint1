# Backlog da Sprint  01

## Dia 01 - Ágil | Onboard

-   Onboard
-   Challenge
-   Entregáveis
-   Orientações Gerais

### ***Organização***
Organização é muito importante porque mantem casa coisa em seu lugar as prioridades na realização de cada tarefa.

A matriz de Eisenhower é uma ferramenta quem por finalidade auxiliar a realização, prioridade e orientar os processos de tomada de decisão nas empresas.

As atividades são categorizadas de acordo com dois critérios: urgência e importância.
-   As tarefas consideradas importantes são aquelas que estão relacionadas ao alncance de determinada meta ou objetivo.
-   Já as tarefas consideradas de urgência estão ligadas ao prazo para a realização e as consequências que pode gerar a empresa.

**Plano Cartesiano**
Em um quadrado faça uma linha horizontal e vertical para dividí-lo em 4 quadrantes. A **linha vertical** representa a importância das tarefas e a **linha horizontal** representa a urgência. 

**Primeiro Quadrante::Tarefas urgentes e importantes**
Essas tarefas são aquelas que merecem prioridade e não podem ser adiadas.

**Segundo Quadrante::Tarefas importantes, mas não urgentes**
Essas tarefas são importantes, mas não precisam ser executadas com carater de urgência; médio e longo prazo.

**Terceiro quadrante::Tarefas urgentes, porém não tão importantes**
Essas tarefas não são muito importantes, mas que precisam ser realizadas com urgência; merecem prioridade, pois o prazo para a conclusão está acabando.

**Quarto Quadrante::Tarefas não importantes e não urgentes**
Essas tarefas não são importantes, nem urgentes; podem ser descartadas ou realizadas ao longo do prazo, sem pressa.

### ***GIT != GITHUB***
O **GIT** é um software de código aberto para versionamento de código; cada versão deselvolvida de um sistema é disponibilizadas aos usuários por meio de versões, ex: v1.0, v2.0, v3.0,... 

Com o GIT é possível retornar a versões anteriores das aplicações desenvolvidas, controlar um time de desenvolvimento sem que um subescreva o código do outro e o mais importante, não salve atualizações dos seus programas desenvolvidos, em pastas num pendrive, cada pasta com uma versão.

O **GITHUB** é um repositório online que serve para guardar o código desenvolviso em sistemas, funciona por meio do GIT, software utilizado para fazer o upload de diretórios na plataforma do Github.

### ***GITLAB***
O **GITLAB** também é um repositório de código online, funciona por meio do GIT para subir diretóros na plataforma.

### ***README***
O readme é um documento de texto, muito usado como documento de orientação para instalação de programas, por exemplo. Utiliza a linguagem de marcação de textos **MARKDOWN**
```md 
    # Títulos
    - Lista
    > Destaque
    1 Lista ordenada
    ** Ênfase em palavras
```