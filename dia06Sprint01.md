# Backlog da Sprint  01

## Dia 06 - MasterClass | Fundamentos do Teste de Software (Back-End)

### Fundamentos do Teste de Software (Back-End)

### ***A Pirâmide de Teste***

A finalidade da pirâmide de testes é definir níveis de testes e apresentar uma noção da quantidade de testes a ser empregado em cada um dos níveis.

**Teste de Unidade**
Na base da pirâmide temos os testes de unidade com a finalidade de verificar o funcionamentosdas huncionalidades da menor estrutura de código testável de um software, independente da integração com outros módulos ou unidades do código todo. É o teste mais rápido de se realizar.

**Teste de Integração**
No meio da pirâmide temos a camada  dos testes de integração, que tem por objetivo testar duas ou mais unidades ou módulos do sistema funcionando em conjunto.

**Teste de Ponta a Ponta**
No topo da pirãmide temos a camada de teste de ponta a ponta que tem por finalidade imitar o comportamento do usuário final. São ambientes que simulam o ambiente o mais próximo do real, como clicar em botões, preencher formulários e verificar se o esperado ocorreu.

A diferença deste tipo de teste para o teste real, é que esse teste é realizado em ambiente controlado com execução de ações por meio de robozinhos ou scripts de testes automatizados.

Esses testes são mais complexos e demanda um tempo maior que os outros tipos de teste e por isso, geralmente sã testes que cobrem apenas os fluxoss principais da aplicação.