# Backlog da Sprint  01

## Dia 08 - MasterClass | AWS Introdução

### AWS Introdução

### ***Princípios Básicos da Nuvem AWS***

**Escalabilidade**
-   **Amazon EC2 Auto Scaling**
    -   Aumenta ou dimuinui o uso dos recusos na nuvem de acordo com a demanda exigida, mais resursos com os picos e menos com baixos picos.
        -   **Scalling Dinãmico**
            -   responde a demanda instantaneamente

        -   **Scaling Preditivo**
            -   a demanda já é prevista e programada

**Elastic Load Balancing**
-   É o serviço AWS que distribui automaticamente o trafego de entrada dos aplicatvos entre varios recursos igualmente.

**Aplictivos Monolíticos**
-   É um bloco que contem 4 componentes interlligados, se um falhar, todos falham.

**Aplicativos Microsserviços**
-   É um bloco que contem 4 componentes separadamente que se complementam, se um falhar, os outros continuam funcionando.

**AWS Lambda**

-   O AWS Lambda é um serviço que permite a execução de códigos sem a necessidade de provisionar ou gerenciar servidores.

**Contêineres**

-   Os contêineres são uma maneira comum de empacotar códigos, configurações e dependências do aplicativo em um único objeto. Você também pode usar contêineres para processos e fluxos de trabalho nos quais há requisitos essenciais de segurança, confiabilidade e escalabilidade.

**Amazon Elastic Container Service (Amazon ECS)**

-   O Amazon Elastic Container Service (Amazon ECS) é um sistema de gerenciamento de contêineres altamente dimensionável e de alto desempenho que permite executar e dimensionar aplicativos em contêineres na AWS.

-   O Amazon ECS é compatível com contêineres Docker. O Docker é uma plataforma de software que permite criar, testar e implantar aplicativos rapidamente.

**Amazon Elastic Kubernetes Service (Amazon EKS)**

-   O Amazon Elastic Kubernetes Service (Amazon EKS) é um serviço totalmente gerenciado que você pode usar para executar o Kubernetes na AWS.

- O Kubernetes é um software de código aberto que permite implantar e gerenciar aplicativos em contêineres em grande escala.

**AWS Fargate**

-   O AWS Fargate é um mecanismo de computação sem servidor para contêineres. Ele funciona com o Amazon ECS e o Amazon EKS.

-   Com o AWS Fargate, você não precisa provisionar ou gerenciar servidores.

 API é uma interface de programação de aplicação.

 CLI utiliza linhas de comando.

 SDKs utiliza linguagens de programação como Java C++, Botnet, Python.

 **Grupos de segurança**

-   Um grupo de segurança é um firewall virtual que controla o tráfego de entrada e saída de uma instância do Amazon EC2.

**Lista de controle de acesso (ACL) de rede**

-   Uma lista de controle de acesso (ACL) de rede é um firewall virtual que controla o tráfego de entrada e saída no nível de sub-rede.