# Backlog da Sprint  01

## Dia 09 - MasterClass | AWS Introdução

### AWS Introdução

### ***Princípios Básicos da Nuvem AWS***

***Modelo de Responsabilidade Compartilhado***
-   A AWS e o cliente tem resposabilidades conjuntas sobre a segurança na nuvem, cada um com as suas particularidades e funções específicas. O cliente é responsável por tudo o que e coloca na nuvem, enquanto a AWS é responsável pela segurança da nuvem.

O usuário **ROOT** tem poder e permissão para fazer o que quiser na nuvem.

**DDOS - Ataque de Negação de Serviço**
-   Indiponibilizar o sistema por sobrecarregamento.

**6R's da Migração**
-   São 6 áreas de negócios disponíveis

    -   Redefinição de hospedagem
    -   Redefinição de plataforma
    -   Refatoração/rearquitetura
    -   Recompra
    -   Retenção
    -   Inativação