# Backlog da Sprint  01

## Dia 02 - Ágil | Planejamento da Sprint 01

-   Planejamento da Sprint 01
-   Entregáveis
-   Orientações Gerais

### SCRUM

### ***Fundamentos do SCRUM Ágil | Rápido e Prático***

O SCRUM é uma ferramenta da metodologia ágil de desenvolvimento de projetos. o scrum possui uma estrutura:
-   **Backlog do produto:** são todos os requisitos e funcionalidades desejas pelo cliente e informadas a equipe de desenvolvimento.

-   **Sprint:** sãp ciclos curtos de desenvolvimento de partes menores do projeto todo; sua duração pode ser de uma a quatro semanas.

-   **Product Owner (Dono do Produto):** É a pessoa ou empresa que solicita o serviço de desenvolvimento de um produto.

-   **Scrum Master:** É o profissional especialista na ferramenta de desenvolvimento de projetos em Scrum, que atua como lider de equipe.

-   **Equipe Scrum:** É a equipe multidisciplinar (profissionais de áreas diferentes, ex: programação, gestão, financeiro, design,...) responsáveis pelo planejamento e desenvolvimento do projeto.
-   **Backlog da Sprint:** são algumas tarefas e funcionalidades escolhidas do Backlog do produto para serem desenvolvidas durantes várias sprintes.

- **Retrospectiva** é a reunião que se faz ao final da sprint para analisar o que foi desenvolvido, o que funcionou, o que não funcionou, o que pode ser melhorado.