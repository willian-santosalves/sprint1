# Backlog da Sprint  01

## Dia 07 - MasterClass | AWS Introdução

### AWS Introdução

### ***Funções Fundamentais em Nuvem***


### ***Princípios Básicos da Nuvem AWS***

A computação em nuvem e a entrega de recursos de TI sob demanda pela internet com uma definição de preço de pagamento conforme você utiliza.

Existem 3 modelos de implantação de computação em nuvem, a computação baseada em nuvem, local e híbrida.

***Implantação Baseada na Nuvem***

-   É o tipo de implantação que oferece a migração de aplicatvos já existentes para a nuvem ou projetar e criar novos aplicativos na nuvem.
    -   ***Benefícios da Computação em Nuvem***
            -   Despesas iniciais variáveis
            -   Economia com a não utilização de data centers
            -   Capacidade contatada conforme demanda
            -   Velocidade e Agilidade
            -   Alcance golbal em minutos

***Implantação Local***

-   Neste modelo os recursos são impalntados localmente usando ferramentas de virtualização e gerenciamento dde recursos.

***Implantação Híbrida***

-   Neste modelo os recursos baseados na nuvem ficam conectados à infraestrutura local. Um complementa a outra.