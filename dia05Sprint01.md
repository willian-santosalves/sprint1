# Backlog da Sprint  01

## Dia 05 - MasterClass | Fundamentos do Teste de Software

### Fundamentos do Teste de Software + Myers e o Pricípio de Pareto

### ***Início Rápido em Teste e QA***
**Análise, Modelagem e Implementação**
Os testes são envolvidos em análises, modelos de teste e técnicas de implementação.

Cenários podem ser simulados, como dia das mães, dia dos namorados, black friday, por exemplo, para poder testar um e-commerce por exemplo, se com o aumento de acessos o site apresenta lentidão, demora para que clientes finalizem pedidos e muito mais.

Ambientes de teste são preparados, podendo ser testes físicos de software em interação com hardware, testes virtualizados por meio de sofwares de virtualização de máquinas, servidores, containeres através do Docker. Com isso situações como falhas e possíveis problemas são analisados.

-   Pirâmide de Testes pode ser implementada.
-   TDD - que é o Desenvolvimento direcioando de Testes
-   BDD - que é o Desenvolvimento Direcionado por Comportamento
-   ATDD -que é o Desenvolvimento Direcionado por Teste de Aceitação

Aqui vai uma seleção e aplicações técnicas de teste como o ISTQB, QAI e TMap Next.

> Segundo Myers, "Testar é analisar um programa com intenção de descobrir erros e defeitos".


**Princípios de Teste de Software**
Os sete princípios do teste de software foram formulados ao longo de 40 anos e suas regras básicas para o processo de teste são:

-   *Teste mostram a presenção de erros:* Nenhum software está livre de defeitos que por ventura venham ocasionar falhas. Não é possivel afirmar que um produto seja isento de falhas. O teste é usado para reduzir os defeitos não encontrados.

-   *Testes Exaustivos são impossíveis:* Fica inviável e exastivo testar um software por completo, pois isso exige tempo e tem um custo elevado, pois possem vários dados de entradas e saída, cenários, combinações e requisitos a serem cumpridos.

-   *Teste Cedo/Antes:* Com o passar do tempo o software vai ficando robsuto e mais caro para se testar, o quanto antes os testes começarem é melhor, assim a quantidade de defeitos pode ser reduzida ao máximo possível e não virar uma "bola de neve".

-   *Aglutinação de Defeitos:* Esse princípio é referido ao Princípio de Pareto, onde aproximadamente 80% dos erros são encontrados em módulos menores do sistema, que representam 20% dos módulos totais do software.

-   *Paradoxo do Pesticida:* Rodar os mesmos testes várias vezes é inútil, visto que, depois que os defeitos são detectados e corrigidos não surte mais efeitos sobre essas funcionalidades corrigidas, logo testes mais elaborados precisarão ser adaptados para encontrar potenciais novas falhas.

-   *Teste Depende do Contexto:* Os tipos e casos de testes vão depender do tipo de negócio que a empresa exerce, cada empresa tem sua regra de negócio e isso traz as sua particularidades, onde um padrão de testes pode não se encaixar numa outra empresa, ex: os testes de uma empresa do setor financeiro não funcionam para uma empresa do setor da industria.

-   *Falácia de Ausência de Erros:*  Nenhum software está imune a defeitos. Os defeitos podem existir e levar o sistema a falhar, assim ele é detectado. Por outro lado, um defeito pode não ocasionar falha e esse defeito persistir com o software até o fim do ciclo de sua vida, desde o desenvolvimento até a sua descontinuação, legado.