# Backlog da Sprint  01

## Dia 04 - MasterClass | Fundamentos do Teste de Software

### Fundamentos do Teste de Software

### ***Início Rápido em Teste e QA***

**Planejamento de Testes**

Quando falamos em testes, logo vem ao pensamento que é só abrir o ambientes de desenvolvimento de testes de produtos e sair testando a suas funcionalidades.

Mas a tarefa de testar produtos precisa de um planejamento, se o objetivo for extrair informações e resultados satisfatórios sobre o produto desenvolvido.

Existem técnicas que podem ser aplicadas para o planejamento de testes, como o *Príncipio de Pareto*, mapas mentais podem ser utilizados, até mesmo o canvas.

Primeiro que todo teste tem o objetivo de entregar resultados, e esses resultados positivos são convertidos nos negócios da empresa, otimizando o tempo e os recursos disponíveis, desde o planejamento do projetos, passando pelas sua estapas, teste e finalização do produto.

Todo teste possui dimensões, onde entram o escopo com a histórias do usuário, cronograma onde são definidos os tempos de trabalho, ciclo de trabalho, as sprints. Teste também tem pocesso de qualidade onde é analisado a viabilidade do projeto, custos e riscos.